<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    protected $table = "artists";
    protected $prmarykey= "AristIId";
    public $timestamps= false;
}
